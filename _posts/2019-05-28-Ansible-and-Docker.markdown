---
layout: post
title:  "Ansible and Docker-CE"
date:   2019-05-28 15:32:14
categories: Docker
---
`Red Hat Ansible` can deploy and manage Docker CE, bellow some Ansible playbooks examples:

install-docker.yml:

{% highlight YAML %}
---
- name: Install Docker-CE
  hosts: docker

  tasks:
  
  - name: Install Docker.
    package:
      name: docker-ce
      state: latest

  - name: Ensure Docker is started and enabled at boot.
    service:
      name: docker
      state: started
      enabled: true

{% endhighlight %}

pull-run-image.yml:

{% highlight YAML %}
---
- name: Pull and Run Hello World Image
  hosts: docker

  tasks:
  
  - name: Pull an image called Hello World
    docker_image:
      name: hello-world
      source: pull

  - name: Run the image called Hello World
    docker_container:
      name: runnig-hello-world
      image: hello-world

{% endhighlight %}

You can see the complete playbooks here: [ansible-docker repository][gitlab-repo], and Ansible's docker modules here: [ansible-docker modules][ansible-modules]

[gitlab-repo]: https://gitlab.com/m.santacruz/ansible-docker-demo
[ansible-modules]: https://docs.ansible.com/ansible/latest/modules/list_of_cloud_modules.html#docker